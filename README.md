# Terveystalo tech-assignment

## Stack

* NodeJS
* ExpressJS
* Swagger
* ReactJs
  * dotenv
  * Parcel bundler
* Material-UI
* Jest

## Project structure

```text
/ -> Mainly configurations
└── src
    ├── api -> API part
    │   ├── measurements -> Endpoint code
    │   └── swagger -> UI and endpoint documentation
    └── ui -> Front-end part
```

## Install needed node modules

```bash
yarn install
  ```

## Local dev environment

First of all, create `.env` file in the project root and add URL to API

like this - this is for local development

```bash
API_URL=http://localhost:3000
```

more info see [here](https://github.com/motdotla/dotenv).

Then type `yarn dev` in your terminal window to start back-end and front-end in one run.

To start API separately type `yarn dev:api` in the terminal.

For front-end part `yarn dev:ui` in a separate terminal window.

API: http://localhost:3000

Swagger documentation: http://localhost:3000/swagger

Front-end: http://localhost:1234

## Run tests

```bash
yarn test
```

## Run linting

```bash
yarn lint
```

## Build for deployment

Update your `.env` with proper API url then run `yarn build`
