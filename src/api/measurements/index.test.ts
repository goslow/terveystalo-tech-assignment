import express from 'express'

const data = [{
  id: '1',
  name: 'Test 1',
  unit: 'Unit 1',
  min: '0',
  max: '10'
}, {
  id: '2',
  name: 'Test 2',
  unit: 'Unit 2',
  min: '11',
  max: '20'
}]
const remove = jest.fn()
const findOne = jest.fn((params) => data.find((item) => item.id === params.id))
const insert = jest.fn()
const update = jest.fn()

jest.mock('lokijs', () => jest.fn().mockReturnValue({
  default: jest.fn(),
  addCollection: jest.fn().mockReturnValue({
    data,
    remove,
    findOne,
    insert,
    update,
  })
}))
jest.mock('express')

import * as measurements from './index';

describe('/measurements endpoint', () => {
  beforeEach(() => {
    jest.clearAllMocks()
    console.error = jest.fn()
  })

  afterAll(() => {
    jest.resetAllMocks()
  })

  test('getAll: should return all records from DB', () => {
    measurements.getAll(express.request, express.response)

    expect(express.response.send).toHaveBeenCalledWith({items: data})
    expect(console.error).not.toHaveBeenCalled()
  });

  test('getOne: should return one record from DB according to search params', () => {
    express.request.params = {id: '1'}

    measurements.getOne(express.request, express.response)

    expect(findOne).toHaveBeenCalledTimes(1)
    expect(express.response.send).toHaveBeenCalledWith(data[0])
    expect(console.error).not.toHaveBeenCalled()
  })

  test('getOne: should return error', () => {
    express.request.params = {id: '3'}

    measurements.getOne(express.request, express.response)

    expect(findOne).toHaveBeenCalledTimes(1)
    expect(express.response.status).toHaveBeenCalledWith(404)
    expect(express.response.json).toHaveBeenCalledWith({'error': 'Item with id: 3 not exists'})
    expect(console.error).toHaveBeenCalledWith('Error: Item with id: 3 not exists')
  })

  test('create: should insert new record', () => {
    express.request.body = {
      name: 'Test 3',
      unit: 'Unit 3',
      min: '10',
      max: '0'
    }

    measurements.create(express.request, express.response)

    expect(insert).toHaveBeenCalledTimes(1)
    expect(express.response.send).toHaveBeenCalledWith('Measurement successfully created')
    expect(console.error).not.toHaveBeenCalled()
  });

  test('create: should return error', () => {
    express.request.body = {}

    measurements.create(express.request, express.response)

    expect(insert).not.toHaveBeenCalled()
    expect(express.response.status).toHaveBeenCalledWith(204)
    expect(express.response.json).toHaveBeenCalledWith({'error': 'Name is mandatory'})
    expect(console.error).toHaveBeenCalledWith('Error: Name is mandatory')

    express.request.body = {
      unit: 'Unit 3',
      min: '10',
      max: '0'
    }

    measurements.create(express.request, express.response)

    expect(insert).not.toHaveBeenCalled()
    expect(express.response.status).toHaveBeenCalledWith(204)
    expect(express.response.json).toHaveBeenCalledWith({'error': 'Name is mandatory'})
    expect(console.error).toHaveBeenCalledWith('Error: Name is mandatory')

    express.request.body = {
      name: 'Test 3',
      min: '10',
      max: '0'
    }

    measurements.create(express.request, express.response)

    expect(insert).not.toHaveBeenCalled()
    expect(express.response.status).toHaveBeenCalledWith(204)
    expect(express.response.json).toHaveBeenCalledWith({'error': 'Unit is mandatory'})
    expect(console.error).toHaveBeenCalledWith('Error: Unit is mandatory')

    express.request.body = {
      name: 'Test 3',
      unit: 'Unit 3',
      max: '0'
    }

    measurements.create(express.request, express.response)

    expect(insert).not.toHaveBeenCalled()
    expect(express.response.status).toHaveBeenCalledWith(204)
    expect(express.response.json).toHaveBeenCalledWith({'error': 'Min value is mandatory'})
    expect(console.error).toHaveBeenCalledWith('Error: Min value is mandatory')

    express.request.body = {
      name: 'Test 3',
      unit: 'Unit 3',
      min: '10',
    }

    measurements.create(express.request, express.response)

    expect(insert).not.toHaveBeenCalled()
    expect(express.response.status).toHaveBeenCalledWith(204)
    expect(express.response.json).toHaveBeenCalledWith({'error': 'Max value is mandatory'})
    expect(console.error).toHaveBeenCalledWith('Error: Max value is mandatory')
  })

  test('update: should update record\'s data', () => {
    express.request.params = {id: '2'}
    express.request.body = {
      name: 'Test 2',
      unit: 'Unit 2',
      min: '10',
      max: '0'
    }

    measurements.update(express.request, express.response)

    expect(findOne).toHaveBeenCalledTimes(1)
    expect(update).toHaveBeenCalledTimes(1)
    expect(express.response.send).toHaveBeenCalledWith('Measurement successfully updated')
    expect(console.error).not.toHaveBeenCalled()
  })

  test('update: should return error', () => {
    express.request.body = {}
    express.request.params = {id: '4'}

    measurements.update(express.request, express.response)

    expect(findOne).toHaveBeenCalledTimes(1)
    expect(update).not.toHaveBeenCalled()
    expect(express.response.status).toHaveBeenCalledWith(404)
    expect(express.response.json).toHaveBeenCalledWith({'error': 'Item with id: 4 not exists'})
    expect(console.error).toHaveBeenCalledWith('Error: Item with id: 4 not exists')

    express.request.params = {id: '2'}

    measurements.update(express.request, express.response)

    expect(findOne).toHaveBeenCalledTimes(2)
    expect(update).not.toHaveBeenCalled()
    expect(express.response.status).toHaveBeenCalledWith(204)
    expect(express.response.json).toHaveBeenCalledWith({'error': 'Should have at least one parameter'})
    expect(console.error).toHaveBeenCalledWith('Error: Should have at least one parameter')
  })

  test('deleteOne: should delete one record from DB', () => {
    express.request.params = {id: '2'}

    measurements.deleteOne(express.request, express.response)

    expect(findOne).toHaveBeenCalledTimes(1)
    expect(remove).toHaveBeenCalledTimes(1)
    expect(express.response.send).toHaveBeenCalledWith('Measurement successfully deleted')
    expect(console.error).not.toHaveBeenCalled()
  });

  test('deleteOne: should return error', () => {
    express.request.params = {id: '99'}

    measurements.deleteOne(express.request, express.response)

    expect(findOne).toHaveBeenCalledTimes(1)
    expect(remove).not.toHaveBeenCalled()
    expect(express.response.status).toHaveBeenCalledWith(404)
    expect(express.response.json).toHaveBeenCalledWith({'error': 'Item with id: 99 not exists'})
    expect(console.error).toHaveBeenCalledWith('Error: Item with id: 99 not exists')
  });
})