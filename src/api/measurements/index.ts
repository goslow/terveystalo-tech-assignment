import loki from 'lokijs'
import { v4 as uuid } from 'uuid'
import { Request, Response } from 'express'

const DB = new loki('database.db')
const measurements = DB.addCollection('measurements')
const errorHandler = (
  error: string,
  res: Response,
  status?: number,
): void => {
  console.error(`Error: ${error}`)

  res.status(status || 500)
  res.json({ error })
  res.send(error)
}

// Preseeded data
measurements.insert({id: uuid(), name: 'Hemoglobiini', unit: 'g/l', min: '134', max: '167'})
measurements.insert({id: uuid(), name: 'LDL-kolesteroli', unit: 'mmol/l', min: '0', max: '3'})

export const getAll = (req: Request, res: Response): void => {
  res.send({items: measurements.data })
}

export const getOne = (req: Request, res: Response): void => {
  const item = measurements.findOne({id: req.params.id});

  if (!item) {
    errorHandler(`Item with id: ${req.params.id} not exists`, res, 404)
  } else {
    res.send(item)
  }
}

export const create = (req: Request, res: Response): void => {
  const {
    name,
    unit,
    min,
    max
  } = req.body;

  if (!name || !unit || !min || !max) {
    if (!name) errorHandler('Name is mandatory', res, 204)
    if (!unit) errorHandler('Unit is mandatory', res, 204)
    if (!min) errorHandler('Min value is mandatory', res, 204)
    if (!max) errorHandler('Max value is mandatory', res, 204)
  } else {
    measurements.insert({
      id: uuid(),
      name,
      unit,
      min,
      max
    })

    res.status(201)
    res.send('Measurement successfully created')
  }
}

export const update = (req: Request, res: Response): void => {
  const item = measurements.findOne({id: req.params.id})
  const isBodyExists = Object.keys(req.body).length > 0

  if (!item || !isBodyExists) {
    if (!item) errorHandler(`Item with id: ${req.params.id} not exists`, res, 404)
    if (!isBodyExists) errorHandler('Should have at least one parameter', res, 204)
  } else {
    measurements.update({ ...item, ...req.body })

    res.send('Measurement successfully updated')
  }
}

export const deleteOne = (req: Request, res: Response): void => {
  const item = measurements.findOne({id: req.params.id})

  if (!item) {
    errorHandler(`Item with id: ${req.params.id} not exists`, res, 404)
  } else {
    measurements.remove(item)

    res.send('Measurement successfully deleted')
  }
}

export default {
  create,
  deleteOne,
  getAll,
  getOne,
  update
}