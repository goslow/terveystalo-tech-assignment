/* eslint-disable no-undef */

window.onload = function() {
  const ui = SwaggerUIBundle({
    url: 'swagger.yaml',
    dom_id: '#swagger-ui',
    deepLinking: true,
    presets: [
      SwaggerUIBundle.presets.apis,
      SwaggerUIStandalonePreset
    ],
    plugins: [
      SwaggerUIBundle.plugins.DownloadUrl
    ],
    layout: 'StandaloneLayout'
  })
  window.ui = ui
  document.querySelector('.topbar').hidden = true
}
