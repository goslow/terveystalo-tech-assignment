export interface MeasurementProps {
  id: string,
  name: string,
  unit: string,
  min: string,
  max: string
}

export interface NewMeasurementProps {
  name: string,
  unit: string,
  min: string,
  max: string
}