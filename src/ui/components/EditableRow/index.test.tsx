import React from 'react'
import {
  fireEvent,
  render,
  RenderResult,
} from '@testing-library/react'
import {
  Grid,
  Table,
  TableBody,
} from '@material-ui/core';

import EditableRow, { EditableRowProps } from './';

let wrapper: RenderResult;

const handleCancel = jest.fn()
const handleMeasurementDataChange = jest.fn()
const handleSubmit = jest.fn()
const defaultProps = {
  handleCancel,
  handleMeasurementDataChange,
  newMeasurementData: {
    name: 'TestName',
    unit: 'TestUnit',
    min: '0',
    max: '100'
  }
} as EditableRowProps
const getWrapper = (newProps = {}) => {
  const props = {
    ...defaultProps,
    ...newProps,
  }

  wrapper = render(
    <Grid
      component="form"
      onSubmit={handleSubmit}
    >
      <Table>
        <TableBody>
          <EditableRow {...props} />
        </TableBody>
      </Table>
    </Grid>
  )
}
describe('<EditableRow />', () => {
  beforeEach(() => {
    handleCancel.mockClear()
    handleMeasurementDataChange.mockClear()
    handleSubmit.mockClear()
    getWrapper()
  });

  afterAll(() => {
    jest.resetAllMocks()
    jest.clearAllMocks()
  });

  test('should match to snapshot (edit)', () => {
    expect(wrapper.asFragment()).toMatchSnapshot()
  })

  test('should match to snapshot (new)', () => {
    getWrapper({
      isNew: true,
      newMeasurementData: {}
    })
    expect(wrapper.asFragment()).toMatchSnapshot()
  })

  test('should handle cancel event', () => {
    const button = wrapper.getByTestId(/cancel-button/i)
    fireEvent.click(button)

    expect(handleCancel).toHaveBeenCalled()
  })

  test('should handle submit event', () => {
    const button = wrapper.getByTestId(/submit-button/i)
    fireEvent.submit(button)

    expect(handleSubmit).toHaveBeenCalled()
  })

  test('should handle change event on `name` field', () => {
    const field = wrapper.getByDisplayValue('TestName')

    fireEvent.change(field, { target: { value: 'a' } })

    expect(handleMeasurementDataChange).toHaveBeenCalled()
  })

  test('should handle change event on `unit` field', () => {
    const field = wrapper.getByDisplayValue('TestUnit')

    fireEvent.change(field, { target: { value: 'a' } })

    expect(handleMeasurementDataChange).toHaveBeenCalled()
  })

  test('should handle change event on `min` field', () => {
    const field = wrapper.getByDisplayValue('0')

    fireEvent.change(field, { target: { value: '1' } })

    expect(handleMeasurementDataChange).toHaveBeenCalled()
  })

  test('should handle change event on `max` field', () => {
    const field = wrapper.getByDisplayValue('100')

    fireEvent.change(field, { target: { value: '0' } })

    expect(handleMeasurementDataChange).toHaveBeenCalled()
  })
})