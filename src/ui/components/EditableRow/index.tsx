import React, {
  ChangeEvent,
  PropsWithChildren
} from 'react'
import {
  Button,
  TableCell,
  TableRow,
  TextField,
} from '@material-ui/core';

import { NewMeasurementProps } from '../../schemas'

export interface EditableRowProps {
  handleCancel: () => void,
  handleMeasurementDataChange: (measurementData: NewMeasurementProps) => void,
  isNew?: boolean,
  newMeasurementData: NewMeasurementProps
}

const EditableRow = (props: PropsWithChildren<EditableRowProps>): JSX.Element => {
  return (
    <TableRow key="newOrEditableRow" data-testid="newOrEditableRow">
      <TableCell component="th" scope="row">
        <TextField
          data-testid="name-field"
          id="name-field"
          label="Name"
          onChange={
            (event: ChangeEvent<HTMLInputElement>) => props.handleMeasurementDataChange({
              ...props.newMeasurementData,
              name: event.target.value
            })
          }
          required
          value={props.newMeasurementData?.name || ''}
        />
      </TableCell>
      <TableCell>
        <TextField
          data-testid="unit-field"
          id="unit-field"
          label="Unit"
          onChange={
            (event: ChangeEvent<HTMLInputElement>) => props.handleMeasurementDataChange({
              ...props.newMeasurementData,
              unit: event.target.value
            })
          }
          required
          value={props.newMeasurementData?.unit || ''}
        />
      </TableCell>
      <TableCell>
        <TextField
          data-testid="min-field"
          id="min-field"
          label="Min value"
          onChange={
            (event: ChangeEvent<HTMLInputElement>) => props.handleMeasurementDataChange({
              ...props.newMeasurementData,
              min: event.target.value
            })
          }
          required
          value={props.newMeasurementData?.min || ''}
        />
      </TableCell>
      <TableCell>
        <TextField
          data-testid="max-field"
          id="max-field"
          label="Max value"
          onChange={
            (event: ChangeEvent<HTMLInputElement>) => props.handleMeasurementDataChange({
              ...props.newMeasurementData,
              max: event.target.value
            })
          }
          required
          value={props.newMeasurementData?.max || ''}
        />
      </TableCell>
      <TableCell>
        <Button
          color="primary"
          data-testid="submit-button"
          type="submit"
        >
          { props.isNew ? 'Create' : 'Update' }
        </Button>
        <Button
          data-testid="cancel-button"
          onClick={props.handleCancel}
        >
          Cancel
        </Button>
      </TableCell>
    </TableRow>
  )
}

export default EditableRow