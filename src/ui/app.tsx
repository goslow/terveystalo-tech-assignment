import React, {
  useEffect,
  useState,
} from 'react'
import { makeStyles } from '@material-ui/core/styles'
import {
  Button,
  Grid,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from '@material-ui/core'
import axios from 'axios'

import {
  MeasurementProps,
  NewMeasurementProps
} from './schemas'
import EditableRow from './components/EditableRow'

const HEADERS = [
  'name',
  'unit',
  'min',
  'max',
  'actions'
]
const axiosInstance = axios.create({
  baseURL: process.env.API_URL
})

const App = () => {
  const [measurements, setMeasurements] = useState<MeasurementProps[]>([])
  const [newButtonPressed, setNewButtonnPressed] = useState<boolean>(false)
  const [editMeasurementId, setEditMeasurementId] = useState<string | undefined>()
  const [newMeasurementData, setNewMeasurementData] = useState<NewMeasurementProps>({} as NewMeasurementProps)

  const fetchData = async (): Promise<void> => {
    try {
      const { data } = await axiosInstance.get('/measurements')

      setMeasurements(data.items || [])
    } catch (error) {
      console.error("Error on data fetch", error);
    }
  }

  useEffect(() => {
    if (measurements.length === 0) fetchData()
  }, [measurements])

  const updateMeasurement = async (): Promise<void> => {
    try {
      await axiosInstance.patch(`/measurements/${editMeasurementId}`, newMeasurementData)

      setNewMeasurementData({} as NewMeasurementProps)
      setEditMeasurementId(undefined)
    } catch (error) {
      console.error("Error on item deletion", error);
    }
  }

  const deleteItem = async (id: string): Promise<void> => {
    try {
      await axiosInstance.delete(`/measurements/${id}`)

      setMeasurements(measurements.filter((item: MeasurementProps) => item.id !== id))
    } catch (error) {
      console.error("Error on item deletion", error);
    }
  }

  const createMeasurement = async (): Promise<void> => {
    try {
      await axiosInstance.post('/measurements', newMeasurementData)
    } catch (error) {
      console.error("Error on new measurement creation", error);
    }
  };

  const handleCancel = (): void => {
    setNewButtonnPressed(false)
    setNewMeasurementData({} as NewMeasurementProps)
    setEditMeasurementId(undefined)
  }

  const useStyles = makeStyles({
    root: {
      minWidth: 650,
    },
    gridItem: {
      width: '100%',
      marginTop: '1rem'
    }
  });

  const classes = useStyles();

  return (
    <Grid
      alignItems="center"
      container
      justify="center"
      className={classes.root}
      component="form"
      onSubmit={newButtonPressed ? createMeasurement : updateMeasurement}
    >
      <Grid
        className={classes.gridItem}
        item
      >
        <Button
          variant="outlined"
          color="primary"
          onClick={() => setNewButtonnPressed(true)}
        >
          New
        </Button>
      </Grid>
      <Grid
        className={classes.gridItem}
        component={Paper}
        item
      >
        <Table className={classes.root} aria-label="simple table">
          <TableHead>
            <TableRow>
              {
                HEADERS.map((title, id) => (
                  <TableCell key={`${title}-${id}`}>{title}</TableCell>
                ))
              }
            </TableRow>
          </TableHead>
          <TableBody>
            {
              newButtonPressed && (
                <EditableRow
                  handleCancel={handleCancel}
                  handleMeasurementDataChange={setNewMeasurementData}
                  isNew
                  newMeasurementData={newMeasurementData}
                />
              )
            }
            {
              measurements.map((measurement) => (
                editMeasurementId && editMeasurementId === measurement.id
                  ? (
                    <EditableRow
                      handleCancel={handleCancel}
                      handleMeasurementDataChange={setNewMeasurementData}
                      key={measurement.id}
                      newMeasurementData={newMeasurementData || measurement}
                    />
                  )
                  : (
                    <TableRow key={measurement.id}>
                      <TableCell component="th" scope="row">
                        {measurement.name}
                      </TableCell>
                      <TableCell>{measurement.unit}</TableCell>
                      <TableCell>{measurement.min}</TableCell>
                      <TableCell>{measurement.max}</TableCell>
                      <TableCell>
                        <Button
                          color="primary"
                          onClick={() => {
                            setNewMeasurementData(measurement)
                            setEditMeasurementId(measurement.id)
                          }}
                        >
                          Edit
                        </Button>
                        <Button
                          color="secondary"
                          onClick={() => deleteItem(measurement.id)}
                        >
                          Delete
                        </Button>
                      </TableCell>
                    </TableRow>
                  )
              ))
            }
          </TableBody>
        </Table>
      </Grid>
    </Grid>
  )
}

export default App