import React from 'react'
import {
  act,
  fireEvent,
  render,
  RenderResult,
  screen,
} from '@testing-library/react'

Object.assign(process.env, {
  API_URL: 'http://localhost:3000'
});

const defaultProps = {}
const axiosGet = jest.fn()
const axiosPost = jest.fn().mockResolvedValue('')
const axiosPatch = jest.fn().mockResolvedValue('')
const axiosDelete = jest.fn().mockResolvedValue('')
const mockedError = jest.fn()

jest.mock('axios', jest.fn().mockReturnValue({
  create: () => ({
    get: axiosGet,
    post: axiosPost,
    patch: axiosPatch,
    delete: axiosDelete
  }),
}))
console.error = mockedError

import App from './app';

let wrapper: RenderResult

const getWrapper = () => {
  act(() => {
    wrapper = render(
      <App {...defaultProps} />
    )
  })
}

describe('<App />', () => {
  beforeEach(() => {
    axiosGet.mockResolvedValue({
      data: {
        items: [
          {
            id: 1,
            name: 'Test',
            unit: 'mb',
            min: '0',
            max: '1'
          }
        ]
      }
    })
    axiosPost.mockClear()
    axiosPatch.mockClear()
    axiosDelete.mockClear()
    mockedError.mockClear()

    getWrapper()
  })

  afterAll(() => {
    jest.resetAllMocks()
    jest.clearAllMocks()
  })

  test('should match to snapshot', () => {
    expect(wrapper.asFragment()).toMatchSnapshot()
  })

  test('should throw error on axios get', () => {
    axiosGet.mockRejectedValue('')

    getWrapper()

    expect(axiosGet).toHaveBeenCalled()
    expect(axiosPost).not.toHaveBeenCalled()
    expect(axiosPatch).not.toHaveBeenCalled()
    expect(axiosDelete).not.toHaveBeenCalled()
    expect(mockedError).toHaveBeenCalled()
  })

  test('click on `New` button should make editable row visible', () => {
    expect(screen.queryByTestId('newOrEditableRow')).toBeNull()

    fireEvent.click(screen.getByText('New'))

    expect(screen.queryByTestId('newOrEditableRow')).not.toBeNull()
  });

  test('click on `Create` should use POST endpoint', () => {
    fireEvent.click(screen.getByText('New'))
    fireEvent.change(wrapper.container.querySelector('#name-field'), { target: { value: 'a' } })
    fireEvent.change(wrapper.container.querySelector('#unit-field'), { target: { value: 'b' } })
    fireEvent.change(wrapper.container.querySelector('#min-field'), { target: { value: 'c' } })
    fireEvent.change(wrapper.container.querySelector('#max-field'), { target: { value: 'd' } })
    fireEvent.click(screen.getByText('Create'))

    expect(axiosPost).toHaveBeenCalledWith(
      '/measurements',
      {
        max: 'd',
        min: 'c',
        name: 'a',
        unit: 'b',
      }
    )
  });

  test('click on `Cancel` should hide editable field', () => {
    fireEvent.click(screen.getByText('New'))
    fireEvent.change(wrapper.container.querySelector('#name-field'), { target: { value: 'a' } })
    fireEvent.click(screen.getByText('Cancel'))

    expect(axiosPost).not.toHaveBeenCalled()
    expect(screen.queryByTestId('newOrEditableRow')).toBeNull()
  });

  test('click on `Edit` should make editable files visible', async () => {
    fireEvent.click(screen.getByText('Edit'))
    expect(screen.queryByTestId('newOrEditableRow')).not.toBeNull()
  });

  test('click on `Update` should call PATCH endpoint', async () => {
    fireEvent.click(screen.getByText('Edit'))
    fireEvent.change(wrapper.container.querySelector('#unit-field'), { target: { value: 'updatedUnit' } })
    fireEvent.click(screen.getByText('Update'))

    expect(axiosPatch).toHaveBeenCalledWith(
      '/measurements/1',
      {
        id: 1,
        name: 'Test',
        unit: 'updatedUnit',
        min: '0',
        max: '1'
      }
    )
  });

  test('click on `Delete` should call DELETE endpoint', async () => {
    fireEvent.click(screen.getByText('Delete'))
    expect(axiosDelete).toHaveBeenCalledWith('/measurements/1')
  })
})