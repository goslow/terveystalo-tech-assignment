import express, {
  Request,
  Response,
  NextFunction
} from 'express'
import path from 'path';
import cors from 'cors';

import measurements from './api/measurements';

const app = express()
const port = process.env.PORT || 3000
const isDev = process.env.NODE_ENV && ['dev', 'development'].includes(process.env.NODE_ENV)
const customAuthorizer = (
  req: Request,
  res: Response,
  next: NextFunction
  ): void => {
    console.log('Authorization should be implemented here...') // eslint-disable-line
    next()
  }

app.use(cors())
app.use(express.urlencoded())
app.use(express.json())
app.use(customAuthorizer)

app.get('/measurements', measurements.getAll)
app.get('/measurements/:id', measurements.getOne)
app.post('/measurements', measurements.create)
app.delete('/measurements/:id', measurements.deleteOne)
app.patch('/measurements/:id', measurements.update)

app.use('/swagger', express.static(path.join(__dirname, 'api/swagger')))


const uiPath = isDev
? path.join(__dirname, '../dist', 'ui') // NOTE: Remember to build app first -> `yarn build` or `yarn build:ui`
: path.join(__dirname, 'ui')
app.use('/ui', express.static(uiPath))
app.use('/', express.static(uiPath))


app.listen(port, () => {
  console.info(`Server run at http://localhost:${port}`)
})